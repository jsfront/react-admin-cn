import Footer from './Common/Footer'
import Navbar from './Default/Navbar'

export default function Layout({ children }: any) {
  return (
    <>
      <Navbar />
      <main className="app-main">{children}</main>
      <Footer />
    </>
  )
}
