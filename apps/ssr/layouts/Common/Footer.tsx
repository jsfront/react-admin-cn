import Link from 'next/link'
import { Link as LinkIcon } from '@carbon/icons-react'
import { useSetState } from 'ahooks'
import { Popover, Space, Tooltip } from 'antd'
import QQ from '@/public/icons/qq.svg'
import Next from '@/public/icons/next.svg'
import Nest from '@/public/icons/nest.svg'
import ReactIcon from '@/public/icons/react.svg'
import WeiXin from '@/public/icons/weixin.svg'
import ReactWx from '@/public/images/react-wx.jpg'
import Image from 'next/image'

export default function Footer() {
  const [state] = useSetState({
    // 列表
    links: [
      { name: 'vue-admin.cn', title: 'vue管理平台', url: 'http://vue-admin.cn' },
      { name: 'ng-admin.cn', title: 'angular管理平台', url: 'http://ng-admin.cn' },
    ],
    year: new Date().getFullYear(),
  })

  // 社会分享
  const share = {
    client: 'https://gitee.com/jsfront/react-admin-cn',
    server: 'https://gitee.com/jsfront/nest-admin-api',
    react: 'https://qm.qq.com/cgi-bin/qm/qr?k=6KpUzxq6gZNkggPRjH_0x4_EVEXv78Ph&jump_from=webapi&authKey=mrt+ksUjX16DZsEzCH5maFsRIuj+/B+PvmczjZuZ+YO31tn96P/uJBC9qM7/DYRw',
    node: 'https://qm.qq.com/cgi-bin/qm/qr?k=-Jr4gqZRj872N5JeKbJaqIrvrlu3maSq&jump_from=webapi&authKey=ZHUuc+ivFOkGyIqx1Mib8wK1UQudDakNhpBwWuDYFUowIFV+HBbJZXDukdw0i1wO',
  }

  const weixinContent = (
    <div className="ui-react-wx animate__animated animate__bounceIn">
      <div className="wechat-qrcode-title text-center mb-2">微信群</div>
      <div className="wechat-qrcode-img">
        <Image width={150} height={130} alt="react管理平台集中地-提交入口" unoptimized src={ReactWx} />
      </div>
    </div>
  )

  return (
    <div className="app-footer">
      <div className="container flex items-center justify-between">
        <div className="app-footer-logo svg-rot">
          <Space size={20}>
            {state.links.map((link) => {
              return (
                <Link key={link.name} href={link.url} title={link.title} target="_blank">
                  <Space size={8}>
                    <LinkIcon />
                    <span>{link.name} </span>
                  </Space>
                </Link>
              )
            })}
          </Space>
        </div>
        <div className="nav text-blue-600">
          <Space size={8}>
            <Tooltip title="前台仓库">
              <Link href={share.client} target="_blank" title="前台仓库">
                <Next />
              </Link>
            </Tooltip>
            <Tooltip title="API仓库">
              <Link href={share.server} target="_blank" title="API仓库">
                <Nest />
              </Link>
            </Tooltip>
            <Tooltip title="React群">
              <Link href={share.react} target="_blank" title="React群">
                <ReactIcon />
              </Link>
            </Tooltip>
            <Tooltip title="Node群">
              <Link href={share.node} target="_blank" title="Node群">
                <QQ />
              </Link>
            </Tooltip>
            <Popover placement="top" content={weixinContent} trigger="hover">
              <a href="###">
                <WeiXin />
              </a>
            </Popover>
          </Space>
        </div>
        <div className="copyright text-xs font-sans">
          <span> © {state.year}. All rights reserved.</span>
        </div>
      </div>
    </div>
  )
}
