import { ROUTES_LINKS } from '@/config'
import { useRouter } from 'next/router'

// 获取navId
export function useNavId() {
  const router = useRouter()
  const routerName = router.route.slice(1)
  const currentNav = ROUTES_LINKS.find((item: any) => item.name === routerName)
  const queryNavId = router.query.navId ? router.query.navId : 1

  return currentNav ? currentNav.id : queryNavId
}
