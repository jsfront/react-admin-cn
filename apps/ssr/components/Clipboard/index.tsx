import React from 'react'
import { Tooltip, message } from 'antd'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import { CopyOutlined } from '@ant-design/icons'

interface IProps {
  text: string
  children?: React.ReactNode
  isShowTip?: boolean
}

/**
 * 复制到剪切板
 */
export default function AppClipboard(props: IProps) {
  const { text, children, isShowTip = false } = props

  const copy = (
    <CopyToClipboard
      text={text}
      onCopy={() => {
        message.success('复制成功！')
      }}
    >
      <span style={{ cursor: 'pointer' }}>{ children || <CopyOutlined />}</span>
    </CopyToClipboard>
  )

  const tip = <Tooltip title="点击复制">{copy}</Tooltip>

  return isShowTip ? tip : copy
}
