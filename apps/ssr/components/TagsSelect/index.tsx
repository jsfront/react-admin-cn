import React, { FC } from 'react'
import { Select } from 'antd'
import { useTags } from '@/hooks/common'

const { Option } = Select

interface IProps {
  onChange?: any
  width?: number | string
  defaultValue?: number
  type?: number
  config?: any
}

const TagsSelect: FC<IProps> = ({ onChange, width, defaultValue, config, type = 2 }) => {
  const { tagsList } = useTags({ params: { type } })
  const innerWidth = width || 120
  const style = config?.style || { width: innerWidth }

  return (
    <Select placeholder="请选择标签" labelInValue onChange={onChange} key={defaultValue} mode="multiple" defaultValue={defaultValue} style={style}>
      {tagsList?.length > 0 && tagsList.map(({ id, tagName }: any) => {
        return (<Option key={id} value={id}>{tagName}</Option>)
      })}
    </Select>
  )
}

export default TagsSelect
