import { Button, Space } from 'antd'
import Link from 'next/link'
import classNames from 'classnames'

type PageProps = {
  title: string
  btnUrl: string
  label?: string
  className: string
  feature: any
}

export default function Banner({ title, btnUrl, label = '提交你的管理模板', className, feature }: PageProps) {
  const onSubmit = () => {
    alert(1)
  }

  return (
    <div className={classNames('app-page-banner', className)}>
      <div className="banner-text">
        <div className="title">{title}</div>
        <div className="feature">
          <Space>
            {feature.map((f: any) => {
              return (
                <span key={f}>{f}</span>
              )
            })}
          </Space>
        </div>
        <div className="contribute">
          {btnUrl ? <Link href={btnUrl} target="_blank"><Button className="admin-btn">{ label }</Button></Link> : <Button onClick={onSubmit}>{ label }</Button>}
        </div>
      </div>
    </div>
  )
}
