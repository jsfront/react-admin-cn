import config from '@/config'
import Link from 'next/link'
import Img from '@/public/images/react.svg'

export default function Logo() {
  return (
    <Link className="rotate-svg flex items-center" href="/" title={config.title}>
      <div className="rounded-full mr-3">
        <Img />
      </div>
      <span className="app-header-title">{config.title}</span>
    </Link>
  )
}
